package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Mahasiswa struct {
	Id      int
	Nama    string
	Fisika  int
	Biologi int
	Kimia   int
	Status  string
}

func main2() {
	var mahasiswa []Mahasiswa

	scanner := bufio.NewScanner(os.Stdin)
	for {
		fmt.Printf("\n")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("\tMENU")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("1. Input Data Mahasiswa")
		fmt.Println("2. Tampil Data Mahasiswa")
		fmt.Println("3. Seleksi Data Mahasiswa > 70 - Lulus")
		fmt.Println("4. Exit")
		fmt.Printf("Masukan pilihan: ")
		scanner.Scan()
		userInput, _ := strconv.ParseInt(scanner.Text(), 10, 64)
		if userInput == 4 {
			fmt.Printf("Program Exited")
			break
		}
		switch userInput {
		case 1:
			inputMahasiswa(&mahasiswa)
		case 2:
			tampilDataMahasiswa(mahasiswa)
		case 3:
			seleksiMahasiswa(&mahasiswa)
		}
	}

}

func inputMahasiswa(mahasiswa *[]Mahasiswa) {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Masukan Id: ")
	scanner.Scan()
	id, _ := strconv.ParseInt(scanner.Text(), 10, 64)
	fmt.Print("Masukan nama: ")
	scanner.Scan()
	nama := scanner.Text()
	fmt.Print("Masukan nilai fisika: ")
	scanner.Scan()
	fisika, _ := strconv.ParseInt(scanner.Text(), 10, 64)
	fmt.Print("Masukan nilai biologi: ")
	scanner.Scan()
	biologi, _ := strconv.ParseInt(scanner.Text(), 10, 64)
	fmt.Print("Masukan nilai kimia: ")
	scanner.Scan()
	kimia, _ := strconv.ParseInt(scanner.Text(), 10, 64)
	mahasiswaNew := Mahasiswa{}
	mahasiswaNew.Id = int(id)
	mahasiswaNew.Nama = nama
	mahasiswaNew.Fisika = int(fisika)
	mahasiswaNew.Biologi = int(biologi)
	mahasiswaNew.Kimia = int(kimia)
	*mahasiswa = append(*mahasiswa, mahasiswaNew)
}

func tampilDataMahasiswa(mahasiswa []Mahasiswa) {
	for _, el := range mahasiswa {
		fmt.Println(stringify(el))
	}
}

func seleksiMahasiswa(mahasiswa *[]Mahasiswa) {
	for i, el := range *mahasiswa {
		if (el.Biologi+el.Fisika+el.Kimia)/3 > 70 {
			(*mahasiswa)[i].Status = "Lulus"
			fmt.Println(stringify((*mahasiswa)[i]))
		} else {
			(*mahasiswa)[i].Status = "Tidak Lulus"
		}
	}
}

// func stringify(data interface{}) string {
// 	b, _ := json.MarshalIndent(data, " ", " ")
// 	return string(b)
// }

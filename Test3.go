package main

import "fmt"

type Shaper interface {
	Area() int
}
type Rectangle struct {
	length, width int
}
type Circle struct {
	radiut int
}

func (r Rectangle) Area() int {
	return r.length * r.width
}

func main3() {
	r := Rectangle{length: 5, width: 3}
	fmt.Println("Rectangle r detail are: ", r)
	fmt.Println("Rectangle r's ares is: ", r.Area())
	s := Shaper(r)
	fmt.Println("Area of the shape r is: ", s.Area())
}
